import asyncio
import logging
import grpc
import jwt

from log4mongo.handlers import BufferedMongoHandler

from api.service.service import deliver

import delivery_pb2
import delivery_pb2_grpc
import orderservice_pb2
import orderservice_pb2_grpc

# Coroutines to be invoked when the event loop is shutting down.
_cleanup_coroutines = []

DELIVERY_GRPC_PORT = 50070
MONGODB_URL = "mongodb+srv://PIZZAKU:Hthg027n2uTubGZa@cluster0.rrcy1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

# Setup logging
logging.basicConfig(level=logging.INFO)
handler = BufferedMongoHandler(
    host=MONGODB_URL, database_name="logging-service", collection="logs"
)
logger = logging.getLogger("delivery-server")
logger.addHandler(handler)
logger.info("starting grpc server")


class Delivery(delivery_pb2_grpc.DeliveryServicer):
    async def Deliver(self, request, context):
        # JWT
        payload = jwt.decode(request.accessToken, options={"verify_signature": False})

        # Logging
        logger.info(
            f'Delivery request received, request: {payload["role"]} - {request.orderId} - {type(request)}'
        )

        if payload["role"] != "chef":
            logger.info("Role invalid")
            return delivery_pb2.DeliverReply(status=403, message="role is not chef")
        res = deliver()
        logger.info(
            f'Changing orderId {request.orderId} status from "SENDING" to "FINISHED"'
        )
        with grpc.insecure_channel("localhost:50053") as channel:
            stub = orderservice_pb2_grpc.OrderServiceStub(channel)
            response = stub.UpdateOrder(
                orderservice_pb2.UpdateOrderRequest(
                    accessToken=request.accessToken,
                    orderId=request.orderId,
                    orderStatus="finished",
                )
            )
        return delivery_pb2.DeliverReply(status=res["status"], message=res["message"])


async def serve():
    server = grpc.aio.server()
    delivery_pb2_grpc.add_DeliveryServicer_to_server(Delivery(), server)
    server.add_insecure_port(f"[::]:{DELIVERY_GRPC_PORT}")
    await server.start()

    async def server_graceful_shutdown():
        logging.info("Starting graceful shutdown...")
        # Shuts down the server with 0 seconds of grace period. During the
        # grace period, the server won't accept new connections and allow
        # existing RPCs to continue within the grace period.
        await server.stop(5)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(serve())
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()
