from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from api.controller.PH_Main import controller

app = FastAPI(title="Circle UI")
app.include_router(controller.router)

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
