import time
from fastapi import status


async def deliver():
    time.sleep(300)
    return {"status": status.HTTP_200_OK, "message": "Pizza delivered"}
